package graphql

import "gitlab.com/alexdouze12/image-store/pkg/image-store/business"

//go:generate gqlgen generate

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	BusiServices *business.Services
}
