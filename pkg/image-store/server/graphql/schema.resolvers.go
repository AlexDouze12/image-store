package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	models1 "gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server/graphql/generated"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server/graphql/model"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server/graphql/utils"
)

func (r *mutationResolver) DeleteImage(ctx context.Context, id string) (*models1.Image, error) {
	// Delete image
	return r.BusiServices.ImageSvc.Delete(ctx, id)
}

func (r *queryResolver) Images(ctx context.Context, after *string, before *string, first *int, last *int, sort *models1.SortOrder, filter *models1.Filter) (*model.ImageConnection, error) {
	// Create pagination input
	pageInput, err := utils.GetPageInput(after, before, first, last)
	// Check error
	if err != nil {
		return nil, err
	}

	// Create projection
	var projection models1.Projection
	// Get projection
	err = utils.ManageConnectionNodeProjection(ctx, &projection)
	// Check error
	if err != nil {
		return nil, err
	}

	// Call business
	list, pageOut, err := r.BusiServices.ImageSvc.GetAllPaginated(ctx, pageInput, sort, filter, &projection)
	// Check error
	if err != nil {
		return nil, err
	}

	// Create connection structure
	var out model.ImageConnection
	// Map connection
	err = utils.MapConnection(&out, list, pageOut)
	// Check error
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (r *queryResolver) Image(ctx context.Context, id string) (*models1.Image, error) {
	// Create projection
	var projection models1.Projection
	// Get projection
	err := utils.ManageSimpleProjection(ctx, &projection)
	// Check error
	if err != nil {
		return nil, err
	}

	// Call business
	return r.BusiServices.ImageSvc.FindByID(ctx, id, &projection)
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
