package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server/graphql/generated"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server/graphql/utils"
)

func (r *imageResolver) CreatedAt(ctx context.Context, obj *models.Image) (string, error) {
	return utils.FormatTime(obj.CreatedAt), nil
}

func (r *imageResolver) UpdatedAt(ctx context.Context, obj *models.Image) (string, error) {
	return utils.FormatTime(obj.UpdatedAt), nil
}

func (r *imageResolver) DownloadURL(ctx context.Context, obj *models.Image) (string, error) {
	// Get download URL
	return r.BusiServices.ImageSvc.GenerateDownloadURL(ctx, obj.ID)
}

// Image returns generated.ImageResolver implementation.
func (r *Resolver) Image() generated.ImageResolver { return &imageResolver{r} }

type imageResolver struct{ *Resolver }
