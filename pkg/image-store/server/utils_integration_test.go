// +build integration

package server

import (
	"gitlab.com/alexdouze12/image-store/pkg/image-store/metrics"
)

// Generate metrics instance
var metricsCtx = metrics.NewMetricsClient()
