package rest

import (
	"context"
	"io"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/thoas/go-funk"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/common/errors"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/common/utils"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
)

func CreateImageHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)

		createInput := &images.CreateInput{}
		err := c.ShouldBind(createInput)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		img, err := imageSvc.Create(c.Request.Context(), createInput)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Answer
		c.JSON(http.StatusAccepted, img)
	}
}

func UploadImageHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)

		// Get image id
		imageID := c.Param("id")

		// Find image
		img, err := imageSvc.FindByID(c.Request.Context(), imageID, nil)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		//Check if image exists
		if img == nil {
			logger.Error("image not found")
			utils.AnswerWithError(c, errors.NewNotFoundError("image not found"))

			return
		}

		// Get file from form
		file, fileHeader, err := c.Request.FormFile("image")
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Create input image
		uploadInput := &images.UploadInput{
			ImageID:     img.ID,
			Body:        file,
			ContentType: fileHeader.Header.Get("Content-Type"),
			ContentSize: fileHeader.Size,
		}

		// Upload image
		img, err = imageSvc.Upload(c.Request.Context(), uploadInput)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Generate download URL
		url, err := imageSvc.GenerateDownloadURL(c.Request.Context(), img.ID)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		out := &ImageOutput{
			Base:        img.Base,
			Name:        img.Name,
			Description: img.Description,
			Status:      img.Status,
			DownloadURL: url,
		}

		// Answer
		c.JSON(http.StatusAccepted, out)
	}
}

func CreateAndUploadImageHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)

		form, err := c.MultipartForm()
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Get file from form
		file, fileHeader, err := c.Request.FormFile("image")
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Create createInput
		createInput := &images.CreateInput{}

		// Check if name is in form
		if form.Value["name"] != nil && len(form.Value["name"]) == 1 {
			createInput.Name = form.Value["name"][0]
		}

		// Check if description is in form
		if form.Value["description"] != nil && len(form.Value["description"]) == 1 {
			createInput.Description = form.Value["description"][0]
		}

		// Default to fileHeader name
		if createInput.Name == "" {
			createInput.Name = fileHeader.Filename
		}

		img, err := imageSvc.Create(c.Request.Context(), createInput)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Create input image
		uploadInput := &images.UploadInput{
			ImageID:     img.ID,
			Body:        file,
			ContentType: fileHeader.Header.Get("Content-Type"),
			ContentSize: fileHeader.Size,
		}

		// Upload image
		img, err = imageSvc.Upload(c.Request.Context(), uploadInput)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Generate download URL
		url, err := imageSvc.GenerateDownloadURL(c.Request.Context(), img.ID)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		out := &ImageOutput{
			Base:        img.Base,
			Name:        img.Name,
			Description: img.Description,
			Status:      img.Status,
			DownloadURL: url,
		}

		// Answer
		c.JSON(http.StatusAccepted, out)
	}
}

func DownloadImageHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)
		// Get image id
		imageID := c.Param("id")
		// Call business
		out, err := imageSvc.Download(c.Request.Context(), imageID)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Answer
		logger.Info("Streaming output on request")
		// Set headers from object
		setHeadersFromObjectOutput(c.Writer, out)
		// Copy data stream to output stream
		_, err = io.Copy(c.Writer, out.Body)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)
		}
	}
}

func ListImagesHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)
		// List images
		imageList, err := imageSvc.GetAll(c.Request.Context())
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		res := make([]*ImageOutput, 0)

		funk.ForEach(imageList, func(image *models.Image) {
			tmp, err := GenerateImageOutputFromImage(c.Request.Context(), imageSvc, image)
			// Check error
			if err != nil {
				logger.Error(err)
			}
			res = append(res, tmp)
		})

		// Answer
		c.JSON(http.StatusOK, res)
	}
}

func DeleteImageHandler(imageSvc images.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get logger
		logger := log.GetLoggerFromGin(c)
		// Get image id
		imageID := c.Param("id")
		deletedImage, err := imageSvc.Delete(c.Request.Context(), imageID)
		// Check error
		if err != nil {
			logger.Error(err)
			utils.AnswerWithError(c, err)

			return
		}

		// Answer
		c.JSON(http.StatusOK, deletedImage)
	}
}

func setHeadersFromObjectOutput(w http.ResponseWriter, obj *images.DownloadOutput) {
	setStrHeader(w, "Cache-Control", obj.CacheControl)
	setStrHeader(w, "Expires", obj.Expires)
	setStrHeader(w, "Content-Disposition", obj.ContentDisposition)
	setStrHeader(w, "Content-Encoding", obj.ContentEncoding)
	setStrHeader(w, "Content-Language", obj.ContentLanguage)
	setIntHeader(w, "Content-Length", obj.ContentLength)
	setStrHeader(w, "Content-Range", obj.ContentRange)
	setStrHeader(w, "Content-Type", obj.ContentType)
	setStrHeader(w, "ETag", obj.ETag)
	setTimeHeader(w, "Last-Modified", obj.LastModified)

	httpStatus := determineHTTPStatus(obj)
	w.WriteHeader(httpStatus)
}

func determineHTTPStatus(obj *images.DownloadOutput) int {
	// Set default http status to 200 OK
	httpStatus := http.StatusOK
	contentRangeIsGiven := len(obj.ContentRange) > 0
	// Check if content will be partial
	if contentRangeIsGiven {
		httpStatus = http.StatusPartialContent
		if totalFileSizeEqualToContentRange(obj) {
			httpStatus = http.StatusOK
		}
	}
	// Return status code
	return httpStatus
}

func totalFileSizeEqualToContentRange(obj *images.DownloadOutput) bool {
	totalSizeIsEqualToContentRange := false
	// Calculate total file size
	totalSize, err := strconv.ParseInt(getFileSizeAsString(obj), 10, 64)
	if err == nil {
		if totalSize == (obj.ContentLength) {
			totalSizeIsEqualToContentRange = true
		}
	}
	// Return result
	return totalSizeIsEqualToContentRange
}

/**
See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Range
*/
func getFileSizeAsString(obj *images.DownloadOutput) string {
	s := strings.Split(obj.ContentRange, "/")
	totalSizeString := s[1]
	totalSizeString = strings.TrimSpace(totalSizeString)
	// Return result
	return totalSizeString
}

func setStrHeader(w http.ResponseWriter, key string, value string) {
	if len(value) > 0 {
		w.Header().Add(key, value)
	}
}

func setIntHeader(w http.ResponseWriter, key string, value int64) {
	if value > 0 {
		w.Header().Add(key, strconv.FormatInt(value, 10))
	}
}

func setTimeHeader(w http.ResponseWriter, key string, value time.Time) {
	if !reflect.DeepEqual(value, time.Time{}) {
		w.Header().Add(key, value.UTC().Format(http.TimeFormat))
	}
}

func GenerateImageOutputFromImage(ctx context.Context, imageSvc images.Service, image *models.Image) (*ImageOutput, error) {
	// Get download URL
	url, err := imageSvc.GenerateDownloadURL(ctx, image.ID)
	// Check error
	if err != nil {
		return nil, err
	}
	return &ImageOutput{
		Base:        image.Base,
		Name:        image.Name,
		Description: image.Description,
		Status:      image.Status,
		DownloadURL: url,
	}, nil
}
