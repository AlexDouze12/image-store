package rest

import "gitlab.com/alexdouze12/image-store/pkg/image-store/database"

type ImageOutput struct {
	database.Base
	Name        string
	Description string
	DownloadURL string
	Status      string
}
