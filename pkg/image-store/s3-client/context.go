package s3client

import (
	"fmt"
	"path"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
)

type s3context struct {
	svcClient  s3iface.S3API
	cfgManager config.Manager
	// Store this to reduce concurrence problem with reload
	s3Cfg *config.S3StorageConfig
}

func (sc *s3context) Initialize(logger log.Logger) error {
	// Get configuration
	s3Cfg := sc.cfgManager.GetConfig().Image.Storage

	sessionConfig := &aws.Config{
		Region: aws.String(s3Cfg.Region),
	}
	// Load credentials if they exists
	if s3Cfg.AccessKey != nil && s3Cfg.SecretKey != nil {
		sessionConfig.Credentials = credentials.NewStaticCredentials(s3Cfg.AccessKey.Value, s3Cfg.SecretKey.Value, "")
	}
	// Load custom endpoint if it exists
	if s3Cfg.S3Endpoint != "" {
		sessionConfig.Endpoint = aws.String(s3Cfg.S3Endpoint)
		sessionConfig.S3ForcePathStyle = aws.Bool(true)
	}
	// Check if ssl needs to be disabled
	if s3Cfg.DisableSSL {
		sessionConfig.DisableSSL = aws.Bool(true)
	}
	// Create session
	sess, err := session.NewSession(sessionConfig)
	if err != nil {
		return err
	}
	// Create s3 client
	svcClient := s3.New(sess)

	// Save it
	sc.svcClient = svcClient
	sc.s3Cfg = s3Cfg

	logger.Debug("S3 configured")

	// Check if bucket exists
	exists, err := sc.bucketExists(s3Cfg.Bucket)
	// Check error
	if err != nil {
		return err
	}
	// Create bucket if it does not exists
	if !exists {
		err2 := sc.createBucket(s3Cfg.Bucket)
		if err2 != nil {
			logger.Error("cannot create bucket")
		}
		logger.Info("Bucket created")
	}

	return nil
}

func (sc *s3context) bucketExists(bucketName string) (bool, error) {
	headBucketInput := &s3.HeadBucketInput{Bucket: aws.String(bucketName)}
	_, err := sc.svcClient.HeadBucket(headBucketInput)
	// Check error
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "NotFound":
				return false, nil
			default:
				fmt.Println(aerr.Code())
				return false, err
			}

		}
		return false, err
	}
	return true, nil
}

func (sc *s3context) createBucket(bucketName string) error {
	// Create bucket
	_, err := sc.svcClient.CreateBucket(&s3.CreateBucketInput{Bucket: aws.String(bucketName)})
	return err
}

func (sc *s3context) GetObject(key string) (*GetOutput, error) {
	// Get prefix key
	pref := sc.s3Cfg.Prefix
	// Concat prefix to key
	rkey := path.Join(pref, key)

	// Get object
	obj, err := sc.svcClient.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(sc.s3Cfg.Bucket),
		Key:    aws.String(rkey),
	})
	// Check error
	if err != nil {
		return nil, err
	}

	// Transform to output
	output := &GetOutput{
		Body: obj.Body,
	}

	if obj.CacheControl != nil {
		output.CacheControl = *obj.CacheControl
	}

	if obj.Expires != nil {
		output.Expires = *obj.Expires
	}

	if obj.ContentDisposition != nil {
		output.ContentDisposition = *obj.ContentDisposition
	}

	if obj.ContentEncoding != nil {
		output.ContentEncoding = *obj.ContentEncoding
	}

	if obj.ContentLanguage != nil {
		output.ContentLanguage = *obj.ContentLanguage
	}

	if obj.ContentLength != nil {
		output.ContentLength = *obj.ContentLength
	}

	if obj.ContentRange != nil {
		output.ContentRange = *obj.ContentRange
	}

	if obj.ContentType != nil {
		output.ContentType = *obj.ContentType
	}

	if obj.ETag != nil {
		output.ETag = *obj.ETag
	}

	if obj.LastModified != nil {
		output.LastModified = *obj.LastModified
	}

	return output, nil
}

func (sc *s3context) PutObject(input *PutInput) error {
	// Get prefix key
	pref := sc.s3Cfg.Prefix
	// Concat prefix to key
	rkey := path.Join(pref, input.Key)
	// Build AWS input object
	inp := &s3.PutObjectInput{
		Body:          input.Body,
		ContentLength: aws.Int64(input.ContentSize),
		Bucket:        aws.String(sc.s3Cfg.Bucket),
		Key:           aws.String(rkey),
	}

	// Manage content type case
	if input.ContentType != "" {
		inp.ContentType = aws.String(input.ContentType)
	}
	// Manage metadata case
	if input.Metadata != nil {
		inp.Metadata = aws.StringMap(input.Metadata)
	}

	// Upload to S3 bucket
	_, err := sc.svcClient.PutObject(inp)
	// Return error
	return err
}

func (sc *s3context) DeleteObject(input *DeleteInput) error {
	// Get prefix key
	pref := sc.s3Cfg.Prefix
	// Concat prefix to key
	rkey := path.Join(pref, input.Key)
	// Build AWS delete object
	inp := &s3.DeleteObjectInput{
		Bucket: aws.String(sc.s3Cfg.Bucket),
		Key:    aws.String(rkey),
	}

	// Delete object
	_, err := sc.svcClient.DeleteObject(inp)
	return err
}
