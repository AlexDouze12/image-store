package s3client

import (
	"io"
	"time"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
)

// Client S3 Context interface.
//go:generate mockgen -destination=./mocks/mock_Client.go -package=mocks gitlab.com/alexdouze12/image-store/pkg/image-store/s3-client Client
type Client interface {
	Initialize(logger log.Logger) error
	GetObject(key string) (*GetOutput, error)
	PutObject(input *PutInput) error
	DeleteObject(input *DeleteInput) error
}

// GetOutput Object output for S3 get object.
type GetOutput struct {
	Body               io.ReadCloser
	CacheControl       string
	Expires            string
	ContentDisposition string
	ContentEncoding    string
	ContentLanguage    string
	ContentLength      int64
	ContentRange       string
	ContentType        string
	ETag               string
	LastModified       time.Time
}

// PutInput Put input object for PUT request.
type PutInput struct {
	Key         string
	Body        io.ReadSeeker
	ContentType string
	ContentSize int64
	Metadata    map[string]string
}

type DeleteInput struct {
	Key string
}

// NewS3Client New S3 Client.
func NewS3Client(cfgManager config.Manager) Client {
	return &s3context{cfgManager: cfgManager}
}
