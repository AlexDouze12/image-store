// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/alexdouze12/image-store/pkg/image-store/tracing (interfaces: Service)

// Package mocks is a generated GoMock package.
package mocks

import (
	context "context"
	graphql "github.com/99designs/gqlgen/graphql"
	gin "github.com/gin-gonic/gin"
	gomock "github.com/golang/mock/gomock"
	opentracing "github.com/opentracing/opentracing-go"
	reflect "reflect"
)

// MockService is a mock of Service interface
type MockService struct {
	ctrl     *gomock.Controller
	recorder *MockServiceMockRecorder
}

// MockServiceMockRecorder is the mock recorder for MockService
type MockServiceMockRecorder struct {
	mock *MockService
}

// NewMockService creates a new mock instance
func NewMockService(ctrl *gomock.Controller) *MockService {
	mock := &MockService{ctrl: ctrl}
	mock.recorder = &MockServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockService) EXPECT() *MockServiceMockRecorder {
	return m.recorder
}

// GetTracer mocks base method
func (m *MockService) GetTracer() opentracing.Tracer {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTracer")
	ret0, _ := ret[0].(opentracing.Tracer)
	return ret0
}

// GetTracer indicates an expected call of GetTracer
func (mr *MockServiceMockRecorder) GetTracer() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTracer", reflect.TypeOf((*MockService)(nil).GetTracer))
}

// GraphqlMiddleware mocks base method
func (m *MockService) GraphqlMiddleware() graphql.HandlerExtension {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GraphqlMiddleware")
	ret0, _ := ret[0].(graphql.HandlerExtension)
	return ret0
}

// GraphqlMiddleware indicates an expected call of GraphqlMiddleware
func (mr *MockServiceMockRecorder) GraphqlMiddleware() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GraphqlMiddleware", reflect.TypeOf((*MockService)(nil).GraphqlMiddleware))
}

// HTTPMiddleware mocks base method
func (m *MockService) HTTPMiddleware(arg0 func(context.Context) string) gin.HandlerFunc {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HTTPMiddleware", arg0)
	ret0, _ := ret[0].(gin.HandlerFunc)
	return ret0
}

// HTTPMiddleware indicates an expected call of HTTPMiddleware
func (mr *MockServiceMockRecorder) HTTPMiddleware(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HTTPMiddleware", reflect.TypeOf((*MockService)(nil).HTTPMiddleware), arg0)
}

// Reload mocks base method
func (m *MockService) Reload() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Reload")
	ret0, _ := ret[0].(error)
	return ret0
}

// Reload indicates an expected call of Reload
func (mr *MockServiceMockRecorder) Reload() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Reload", reflect.TypeOf((*MockService)(nil).Reload))
}
