package database

import (
	"database/sql"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/metrics"
	"gorm.io/gorm"
)

//go:generate mockgen -destination=./mocks/mock_DB.go -package=mocks gitlab.com/alexdouze12/image-store/pkg/image-store/database DB
type DB interface {
	// Get Gorm db object.
	GetGormDB() *gorm.DB
	// Get SQL db object.
	GetSQLDB() (*sql.DB, error)
	// Connect to database.
	Connect() error
	// Close database connection.
	Close() error
	// Ping database.
	Ping() error
	// Reconnect to database.
	Reconnect() error
}

// NewDatabase will generate a new DB object.
func NewDatabase(
	connectionName string,
	cfgManager config.Manager,
	logger log.Logger,
	metricsCl metrics.Client,
) DB {
	return &postresdb{
		logger:         logger,
		cfgManager:     cfgManager,
		metricsCl:      metricsCl,
		connectionName: connectionName,
	}
}
