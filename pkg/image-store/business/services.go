package business

import (
	"gitlab.com/alexdouze12/image-store/pkg/image-store/authx/authorization"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/lockdistributor"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
	s3client "gitlab.com/alexdouze12/image-store/pkg/image-store/s3-client"
)

type Services struct {
	db           database.DB
	systemLogger log.Logger
	ImageSvc     images.Service
}

func (s *Services) MigrateDB() error {
	// Migrate Image Svc
	err := s.ImageSvc.MigrateDB(s.systemLogger)
	if err != nil {
		return err
	}

	return nil
}

func NewServices(systemLogger log.Logger, cfgManager config.Manager, db database.DB, authSvc authorization.Service, ld lockdistributor.Service, s3Svc s3client.Client) *Services {
	// Create images service
	imageSvc := images.NewService(db, authSvc, cfgManager, s3Svc)

	return &Services{
		db:           db,
		systemLogger: systemLogger,
		ImageSvc:     imageSvc,
	}
}
