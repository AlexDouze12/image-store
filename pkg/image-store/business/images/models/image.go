package models

import "gitlab.com/alexdouze12/image-store/pkg/image-store/database"

const StatusUploaded = "UPLOADED"
const StatusNotUploaded = "NOT_UPLOADED"

type Image struct {
	database.Base
	Name        string `gorm:"type:varchar(250)"`
	Description string `gorm:"type:varchar(2000)"`
	Status      string
}
