package models

import "gitlab.com/alexdouze12/image-store/pkg/image-store/database/common"

type SortOrder struct {
	CreatedAt   *common.SortOrderEnum `dbfield:"created_at"`
	UpdatedAt   *common.SortOrderEnum `dbfield:"updated_at"`
	Name        *common.SortOrderEnum `dbfield:"name"`
	Description *common.SortOrderEnum `dbfield:"description"`
	Status      *common.SortOrderEnum `dbfield:"status"`
}

type Filter struct {
	AND         []*Filter
	OR          []*Filter
	CreatedAt   *common.DateFilter    `dbfield:"created_at"`
	UpdatedAt   *common.DateFilter    `dbfield:"updated_at"`
	Name        *common.GenericFilter `dbfield:"name"`
	Description *common.GenericFilter `dbfield:"description"`
	Status      *common.GenericFilter `dbfield:"status"`
}

type Projection struct {
	ID          bool `dbfield:"id" graphqlfield:"id"`
	CreatedAt   bool `dbfield:"created_at" graphqlfield:"createdAt"`
	UpdatedAt   bool `dbfield:"updated_at" graphqlfield:"updatedAt"`
	Name        bool `dbfield:"name" graphqlfield:"name"`
	Description bool `dbfield:"description" graphqlfield:"description"`
	Status      bool `dbfield:"status" graphqlfield:"status"`
}
