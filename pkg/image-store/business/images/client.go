package images

import (
	"context"
	"io"
	"time"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/daos"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/pagination"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
	s3client "gitlab.com/alexdouze12/image-store/pkg/image-store/s3-client"
)

type authorizationService interface {
	CheckAuthorized(ctx context.Context, action, resource string) error
}

type Service interface {
	MigrateDB(systemLogger log.Logger) error
	GetAllPaginated(
		ctx context.Context,
		page *pagination.PageInput,
		sort *models.SortOrder,
		filter *models.Filter,
		projection *models.Projection,
	) ([]*models.Image, *pagination.PageOutput, error)
	GetAll(ctx context.Context) ([]*models.Image, error)
	FindByID(ctx context.Context, id string, projection *models.Projection) (*models.Image, error)
	Create(ctx context.Context, inp *CreateInput) (*models.Image, error)
	Upload(ctx context.Context, inp *UploadInput) (*models.Image, error)
	Download(ctx context.Context, id string) (*DownloadOutput, error)
	GenerateDownloadURL(ctx context.Context, imageID string) (string, error)
	Delete(ctx context.Context, id string) (*models.Image, error)
}

type CreateInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type UploadInput struct {
	ImageID     string
	Body        io.ReadSeeker
	ContentType string
	ContentSize int64
}

type DownloadOutput struct {
	Body               io.ReadCloser
	CacheControl       string
	Expires            string
	ContentDisposition string
	ContentEncoding    string
	ContentLanguage    string
	ContentLength      int64
	ContentRange       string
	ContentType        string
	ETag               string
	LastModified       time.Time
}

func NewService(db database.DB, authSvc authorizationService, cfgManager config.Manager, s3Svc s3client.Client) Service {
	// Create dao
	dao := daos.NewDao(db)

	return &service{dao: dao, authSvc: authSvc, s3Svc: s3Svc, cfgManager: cfgManager}
}
