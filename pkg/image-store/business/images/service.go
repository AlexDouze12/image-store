package images

import (
	"context"
	"fmt"
	"net/url"
	"path"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/daos"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/common/errors"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/pagination"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
	s3client "gitlab.com/alexdouze12/image-store/pkg/image-store/s3-client"
)

const mainAuthorizationPrefix = "image"

type service struct {
	dao        daos.Dao
	authSvc    authorizationService
	s3Svc      s3client.Client
	cfgManager config.Manager
}

func (s *service) MigrateDB(systemLogger log.Logger) error {
	systemLogger.Debug("Migrate database for Images")

	return s.dao.MigrateDB()
}

func (s *service) FindByID(ctx context.Context, id string, projection *models.Projection) (*models.Image, error) {
	// Check authorization
	err := s.authSvc.CheckAuthorized(
		ctx,
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Get"),
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, id),
	)
	// Check error
	if err != nil {
		return nil, err
	}

	// Find by id
	res, err := s.dao.FindByID(id, projection)
	// Check error
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *service) GetAllPaginated(
	ctx context.Context,
	page *pagination.PageInput,
	sort *models.SortOrder,
	filter *models.Filter,
	projection *models.Projection,
) ([]*models.Image, *pagination.PageOutput, error) {
	// Check authorization
	err := s.authSvc.CheckAuthorized(
		ctx,
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "List"),
		"",
	)
	// Check error
	if err != nil {
		return nil, nil, err
	}

	return s.dao.GetAllPaginated(page, sort, filter, projection)
}

func (s *service) GetAll(ctx context.Context) ([]*models.Image, error) {
	// Check authorization
	err := s.authSvc.CheckAuthorized(
		ctx,
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "List"),
		"",
	)
	// Check error
	if err != nil {
		return nil, err
	}

	return s.dao.GetAll()
}

func (s *service) Create(ctx context.Context, inp *CreateInput) (*models.Image, error) {
	// Check authorization
	err := s.authSvc.CheckAuthorized(
		ctx,
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Create"),
		"",
	)
	// Check error
	if err != nil {
		return nil, err
	}

	// Create imgae in db
	return s.dao.CreateOrUpdate(&models.Image{
		Name:        inp.Name,
		Description: inp.Description,
		Status:      models.StatusNotUploaded,
	})
}

func (s *service) SetStatusUploaded(ctx context.Context, id string) (*models.Image, error) {
	// Get image from db
	image, err := s.dao.FindByID(id, nil)
	// Check error
	if err != nil {
		return nil, err
	}

	// Check if image exists
	if image == nil {
		return nil, errors.NewNotFoundError("image not found")
	}

	// Set status to uploaded
	image.Status = models.StatusUploaded

	// Save
	return s.dao.CreateOrUpdate(image)
}

func (s *service) Upload(ctx context.Context, inp *UploadInput) (*models.Image, error) {
	// Get logger
	logger := log.GetLoggerFromContext(ctx)
	// Check authorization
	err := s.authSvc.CheckAuthorized(
		ctx,
		fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Upload"),
		"",
	)
	// Check error
	if err != nil {
		return nil, err
	}

	// Get image from db
	image, err := s.dao.FindByID(inp.ImageID, nil)
	// Check error
	if err != nil {
		return nil, err
	}

	// Check if image exists
	if image == nil {
		return nil, errors.NewNotFoundError("image not found")
	}

	logger.Infof("Trying to save image at path %s on storage", inp.ImageID)
	err = s.s3Svc.PutObject(&s3client.PutInput{
		Key:         image.ID,
		Body:        inp.Body,
		ContentType: inp.ContentType,
		ContentSize: inp.ContentSize,
	})
	// Check error
	if err != nil {
		return nil, err
	}
	logger.Info("Saving file successfully")

	// Update status
	image, err = s.SetStatusUploaded(ctx, image.ID)
	// Check error
	if err != nil {
		return nil, err
	}

	return image, nil
}
func (s *service) Download(ctx context.Context, id string) (*DownloadOutput, error) {
	// Get image from db
	image, err := s.dao.FindByID(id, nil)
	// Check error
	if err != nil {
		return nil, err
	}

	// Check if image exists
	if image == nil {
		return nil, errors.NewNotFoundError("image does not exist")
	}

	// Get file from storage
	out, err := s.s3Svc.GetObject(image.ID)
	// Check error
	if err != nil {
		return nil, err
	}

	// Create output
	res := &DownloadOutput{
		Body:               out.Body,
		CacheControl:       out.CacheControl,
		ContentEncoding:    out.ContentEncoding,
		ContentLanguage:    out.ContentLanguage,
		ContentLength:      out.ContentLength,
		ContentRange:       out.ContentRange,
		ContentType:        out.ContentType,
		ETag:               out.ETag,
		Expires:            out.Expires,
		LastModified:       out.LastModified,
		ContentDisposition: fmt.Sprintf("attachment; filename=\"%s\"", image.Name),
	}

	return res, nil

}

func (s *service) Delete(ctx context.Context, id string) (*models.Image, error) {
	// Get image from db
	image, err := s.FindByID(ctx, id, nil)
	// Check error
	if err != nil {
		return nil, err
	}
	// Check if image exists
	if image == nil {
		return nil, errors.NewNotFoundError("image not found")
	}

	// Delete s3 object
	err = s.s3Svc.DeleteObject(&s3client.DeleteInput{Key: image.ID})
	// Check error
	if err != nil {
		return nil, err
	}

	// Delete db object
	image, err = s.dao.Delete(id, nil)
	// Check error
	if err != nil {
		return nil, err
	}

	return image, nil
}

func (s *service) GenerateDownloadURL(ctx context.Context, imageID string) (string, error) {
	// Get image from db
	image, err := s.FindByID(ctx, imageID, nil)
	// Check error
	if err != nil {
		return "", err
	}
	// Check if image exists
	if image == nil {
		return "", errors.NewNotFoundError("image not found")
	}

	// Check if image is uploaded
	if image.Status != models.StatusUploaded {
		return "", nil
	}

	// Get base url
	burlStr := s.cfgManager.GetConfig().Image.BaseURL
	// Pase base url
	bu, err := url.Parse(burlStr)
	// Check error
	if err != nil {
		return "", err
	}
	// Concat path
	bu.Path = path.Join(bu.Path, s.cfgManager.GetConfig().Image.DownloadPathPrefix, imageID)

	return bu.String(), nil
}
