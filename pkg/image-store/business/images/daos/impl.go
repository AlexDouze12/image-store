package daos

import (
	"errors"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	cerrors "gitlab.com/alexdouze12/image-store/pkg/image-store/common/errors"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/common"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/pagination"
	"gorm.io/gorm"
)

type dao struct {
	db database.DB
}

func (d *dao) MigrateDB() error {
	// Get gorm database
	gdb := d.db.GetGormDB()
	// Migrate
	err := gdb.AutoMigrate(&models.Image{})

	return err
}

func (d *dao) FindByID(id string, projection *models.Projection) (*models.Image, error) {
	// Get gorm db
	db := d.db.GetGormDB()
	// result
	res := &models.Image{}
	// Apply projection
	db, err := common.ManageProjection(projection, db)
	// Check error
	if err != nil {
		return nil, err
	}
	// Find in db
	dbres := db.Where("id = ?", id).First(res)

	// Check error
	err = dbres.Error
	if err != nil {
		// Check if it is a not found error
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}

		return nil, err
	}

	return res, nil
}

func (d *dao) CreateOrUpdate(tt *models.Image) (*models.Image, error) {
	// Get gorm db
	db := d.db.GetGormDB()
	dbres := db.Save(tt)

	// Check error
	err := dbres.Error
	if err != nil {
		return nil, err
	}

	// Return result
	return tt, nil
}

func (d *dao) GetAllPaginated(
	page *pagination.PageInput,
	sort *models.SortOrder,
	filter *models.Filter,
	projection *models.Projection,
) ([]*models.Image, *pagination.PageOutput, error) {
	// Get gorm db
	db := d.db.GetGormDB()
	// result
	res := make([]*models.Image, 0)
	// Find images
	pageOut, err := pagination.Paging(&res, &pagination.PagingOptions{
		DB:         db,
		PageInput:  page,
		Sort:       sort,
		Filter:     filter,
		Projection: projection,
		ExtraFunc:  nil,
	})
	// Check error
	if err != nil {
		return nil, nil, err
	}

	return res, pageOut, nil
}

func (d *dao) GetAll() ([]*models.Image, error) {
	// Get gorm db
	db := d.db.GetGormDB()
	// result
	res := make([]*models.Image, 0)
	// Find images
	dbres := db.Find(&res)
	// Check error
	err := dbres.Error
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (d *dao) Delete(id string, projection *models.Projection) (*models.Image, error) {
	// Finc image
	image, err := d.FindByID(id, projection)
	// Check error
	if err != nil {
		return nil, err
	}
	// Check if image exists
	if image == nil {
		return nil, cerrors.NewNotFoundError("image not found")
	}
	// Get gorm db
	db := d.db.GetGormDB()

	dbres := db.Unscoped().Delete(&image)
	// Check error
	err = dbres.Error
	if err != nil {
		return nil, err
	}
	return image, nil
}
