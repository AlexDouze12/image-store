package daos

import (
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/pagination"
)

//go:generate mockgen -destination=./mocks/mock_Dao.go -package=mocks gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/daos Dao
type Dao interface {
	MigrateDB() error
	GetAllPaginated(
		page *pagination.PageInput,
		sort *models.SortOrder,
		filter *models.Filter,
		projection *models.Projection,
	) ([]*models.Image, *pagination.PageOutput, error)
	CreateOrUpdate(tt *models.Image) (*models.Image, error)
	GetAll() ([]*models.Image, error)
	FindByID(id string, projection *models.Projection) (*models.Image, error)
	Delete(id string, projection *models.Projection) (*models.Image, error)
}

func NewDao(db database.DB) Dao {
	return &dao{db: db}
}
