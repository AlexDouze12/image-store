package images

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	authmock "gitlab.com/alexdouze12/image-store/pkg/image-store/authx/authorization/mocks"
	daomock "gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/daos/mocks"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business/images/models"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database/pagination"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
)

func Test_service_FindByID(t *testing.T) {
	type daoFindByIdMock struct {
		inputID     string
		err         error
		imageOutput *models.Image
	}
	type authCheckAuthorizedMock struct {
		action    string
		resources string
		err       error
	}
	type args struct {
		id string
	}
	tests := []struct {
		daoFindByIdMock         daoFindByIdMock
		authCheckAuthorizedMock authCheckAuthorizedMock
		name                    string
		args                    args
		want                    *models.Image
		wantErr                 bool
		errString               string
	}{
		{
			name: "check authorization error",
			args: args{
				id: "test",
			},
			authCheckAuthorizedMock: authCheckAuthorizedMock{
				action:    fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Get"),
				resources: fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "test"),
				err:       errors.New("authorization error"),
			},
			wantErr:   true,
			errString: "authorization error",
		},
		{
			name: "dao find by id error",
			args: args{
				id: "test",
			},
			authCheckAuthorizedMock: authCheckAuthorizedMock{
				action:    fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Get"),
				resources: fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "test"),
			},
			daoFindByIdMock: daoFindByIdMock{
				inputID: "test",
				err:     errors.New("dao error"),
			},
			wantErr:   true,
			errString: "dao error",
		},
		{
			name: "nominal",
			args: args{
				id: "test",
			},
			authCheckAuthorizedMock: authCheckAuthorizedMock{
				action:    fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "Get"),
				resources: fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "test"),
			},
			daoFindByIdMock: daoFindByIdMock{
				inputID: "test",
				imageOutput: &models.Image{
					Base: database.Base{
						ID: "test",
					},
					Name:        "name",
					Description: "description",
				},
			},
			want: &models.Image{
				Base: database.Base{
					ID: "test",
				},
				Name:        "name",
				Description: "description",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create mock
			ctrl := gomock.NewController(t)
			daoMock := daomock.NewMockDao(ctrl)
			authMock := authmock.NewMockService(ctrl)

			// Add logger
			ctx := log.SetLoggerToContext(context.Background(), log.NewLogger())

			authMock.EXPECT().CheckAuthorized(ctx, tt.authCheckAuthorizedMock.action, tt.authCheckAuthorizedMock.resources).Return(tt.authCheckAuthorizedMock.err)
			daoMock.EXPECT().FindByID(tt.daoFindByIdMock.inputID, nil).Return(tt.daoFindByIdMock.imageOutput, tt.daoFindByIdMock.err)

			s := &service{
				dao:        daoMock,
				authSvc:    authMock,
				s3Svc:      nil,
				cfgManager: nil,
			}
			got, err := s.FindByID(ctx, tt.args.id, nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("service.FindByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && err.Error() != tt.errString {
				t.Errorf("service.FindById() error = %v, wantErr content %v", err, tt.errString)
			}
			if tt.want != nil {
				// Test only field by field
				assert.Equal(t, tt.want.ID, got.ID)
				assert.Equal(t, tt.want.Name, got.Name)
				assert.Equal(t, tt.want.Description, got.Description)
			}
		})
	}
}

func Test_service_GetAllPaginated(t *testing.T) {
	type daoGetAllPaginatedMock struct {
		err          error
		imagesOutput []*models.Image
	}
	type authCheckAuthorizedMock struct {
		err error
	}
	tests := []struct {
		name                    string
		daoGetAllPaginatedMock  daoGetAllPaginatedMock
		authCheckAuthorizedMock authCheckAuthorizedMock
		want                    []*models.Image
		want1                   *pagination.PageOutput
		wantErr                 bool
		errString               string
	}{
		{
			name: "check authorization error",
			authCheckAuthorizedMock: authCheckAuthorizedMock{
				err: errors.New("authorization error"),
			},
			wantErr:   true,
			errString: "authorization error",
		},
		{
			name: "check dao GetAllPaginated error",
			daoGetAllPaginatedMock: daoGetAllPaginatedMock{
				err: errors.New("dao error"),
			},
			wantErr:   true,
			errString: "dao error",
		},
		{
			name: "nominal",
			daoGetAllPaginatedMock: daoGetAllPaginatedMock{
				imagesOutput: []*models.Image{
					{
						Base: database.Base{
							ID: "test",
						},
						Name:        "name",
						Description: "description",
					},
				},
			},
			wantErr: false,
			want: []*models.Image{
				{
					Base: database.Base{
						ID: "test",
					},
					Name:        "name",
					Description: "description",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create mock
			ctrl := gomock.NewController(t)
			daoMock := daomock.NewMockDao(ctrl)
			authMock := authmock.NewMockService(ctrl)

			// Add logger
			ctx := log.SetLoggerToContext(context.Background(), log.NewLogger())

			authMock.EXPECT().CheckAuthorized(ctx, fmt.Sprintf("%s:%s", mainAuthorizationPrefix, "List"), "").Return(tt.authCheckAuthorizedMock.err)
			daoMock.EXPECT().GetAllPaginated(nil, nil, nil, nil).Return(tt.daoGetAllPaginatedMock.imagesOutput, nil, tt.daoGetAllPaginatedMock.err)

			s := &service{
				dao:        daoMock,
				authSvc:    authMock,
				s3Svc:      nil,
				cfgManager: nil,
			}
			got, _, err := s.GetAllPaginated(ctx, nil, nil, nil, nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("service.FindByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && err.Error() != tt.errString {
				t.Errorf("service.FindById() error = %v, wantErr content %v", err, tt.errString)
			}
			if tt.want != nil {
				// Test only field by field
				assert.Equal(t, tt.want[0].ID, got[0].ID)
				assert.Equal(t, tt.want[0].Name, got[0].Name)
				assert.Equal(t, tt.want[0].Description, got[0].Description)
			}
		})
	}
}
