package config

import "gitlab.com/alexdouze12/image-store/pkg/image-store/log"

// Manager.
//go:generate mockgen -destination=./mocks/mock_Manager.go -package=mocks gitlab.com/alexdouze12/image-store/pkg/image-store/config Manager
type Manager interface {
	// Load configuration
	Load() error
	// Get configuration object
	GetConfig() *Config
	// Add on change hook for configuration change
	AddOnChangeHook(hook func())
}

func NewManager(logger log.Logger) Manager {
	return &managercontext{logger: logger}
}
