package main

import (
	"time"

	"gitlab.com/alexdouze12/image-store/pkg/image-store/authx/authentication"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/authx/authorization"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/business"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/config"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/database"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/email"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/lockdistributor"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/log"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/metrics"
	s3client "gitlab.com/alexdouze12/image-store/pkg/image-store/s3-client"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/server"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/tracing"
	"gitlab.com/alexdouze12/image-store/pkg/image-store/version"
	"golang.org/x/sync/errgroup"
)

func main() {
	// Create new logger
	logger := log.NewLogger()

	// Create configuration manager
	cfgManager := config.NewManager(logger)

	// Load configuration
	err := cfgManager.Load()
	if err != nil {
		logger.WithError(err).Fatal(err)
	}

	// Get configuration
	cfg := cfgManager.GetConfig()
	// Configure logger
	err = logger.Configure(cfg.Log.Level, cfg.Log.Format, cfg.Log.FilePath)
	if err != nil {
		logger.WithError(err).Fatal(err)
	}

	// Watch change for logger (special case)
	cfgManager.AddOnChangeHook(func() {
		// Get configuration
		cfg := cfgManager.GetConfig()
		// Configure logger
		err = logger.Configure(cfg.Log.Level, cfg.Log.Format, cfg.Log.FilePath)
		if err != nil {
			logger.WithError(err).Error(err)
		}
	})

	// Getting version
	v := version.GetVersion()
	logger.Infof("Starting version: %s (git commit: %s) built on %s", v.Version, v.GitCommit, v.BuildDate)

	// Create metrics client
	metricsCl := metrics.NewMetricsClient()

	// Generate tracing service instance
	tracingSvc, err := tracing.New(cfgManager, logger)
	// Check error
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	// Prepare on reload hook
	cfgManager.AddOnChangeHook(func() {
		err = tracingSvc.Reload()
		if err != nil {
			logger.WithError(err).Fatal(err)
		}
	})

	// Create database service
	db := database.NewDatabase("main", cfgManager, logger, metricsCl)
	// Connect to engine
	err = db.Connect()
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	// Add configuration reload hook
	cfgManager.AddOnChangeHook(func() {
		err = db.Reconnect()
		if err != nil {
			logger.WithError(err).Fatal(err)
		}
	})

	// Create new mail service
	mailSvc := email.NewService(cfgManager, logger)
	// Try to connect
	err = mailSvc.Initialize()
	if err != nil {
		logger.Fatal(err)
	}
	// Add configuration reload hook
	cfgManager.AddOnChangeHook(func() {
		err = mailSvc.Initialize()
		if err != nil {
			logger.WithError(err).Fatal(err)
		}
	})

	// Create lock distributor service
	ld := lockdistributor.NewService(cfgManager, db)
	// Initialize lock distributor
	err = ld.Initialize(logger)
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	// Add configuration reload hook
	cfgManager.AddOnChangeHook(func() {
		err = ld.Initialize(logger)
		if err != nil {
			logger.WithError(err).Fatal(err)
		}
	})

	// Create S3 client
	s3cl := s3client.NewS3Client(cfgManager)
	// Initialize it
	err = s3cl.Initialize(logger)
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	// Add configuration reload hook
	cfgManager.AddOnChangeHook(func() {
		err = s3cl.Initialize(logger)
		if err != nil {
			logger.WithError(err).Fatal(err)
		}
	})

	// Create authentication service
	authoSvc := authorization.NewService(cfgManager)

	// Create business services
	busServices := business.NewServices(logger, cfgManager, db, authoSvc, ld, s3cl)

	// Migrate database
	err = busServices.MigrateDB()
	if err != nil {
		logger.WithError(err).Fatal(err)
	}

	// Create authentication service
	authenticationSvc := authentication.NewService(cfgManager)

	// Create servers
	svr := server.NewServer(logger, cfgManager, metricsCl, tracingSvc, busServices, authenticationSvc, authoSvc)
	intSvr := server.NewInternalServer(logger, cfgManager, metricsCl)

	// Add checker for database
	intSvr.AddChecker(&server.CheckerInput{
		Name:     "database",
		CheckFn:  db.Ping,
		Interval: 2 * time.Second, //nolint:gomnd // Won't do a const for that
	})
	// Add checker for email service
	intSvr.AddChecker(&server.CheckerInput{
		Name:    "email",
		CheckFn: mailSvc.Check,
		// Interval is long because it takes a lot of time to connect SMTP server (can be 1 second).
		// Moreover, connect 6 time per minute should be ok.
		Interval: 10 * time.Second, //nolint:gomnd // Won't do a const for that
	})

	// Generate server
	err = svr.GenerateServer()
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	// Generate internal server
	err = intSvr.GenerateServer()
	if err != nil {
		logger.WithError(err).Fatal(err)
	}

	var g errgroup.Group

	g.Go(svr.Listen)
	g.Go(intSvr.Listen)

	if err := g.Wait(); err != nil {
		logger.WithError(err).Fatal(err)
	}
}
