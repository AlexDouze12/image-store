# Image Store

This repo contains code for a image store REST API on top of a Minio storage backend.
The API allows to:
- Upload an image with optional name and description.
- List stored images. For each image you should be able to retrieve: downloadURL, name, description.
- Download an image with its downloadURL
- Delete an image

## Run the project

In this project there is a Makefile to interact with the project. Here are the main command to build and run the project:

- `make setup/services` to start the docker-compose stack with sidecar services (minio, postgres, keycloak, opa)
- `make code/build`to build the Golang project. An executable is created in `./bin/image-store`

Once you ran the two previous command to can run:
```
./bin/image-store
```


## APIs
### Rest API

The Rest API is served under `/api/rest/*`. Here are the available subpath and method available:

- `POST /api/rest/image/full` to post an image, the `Content-Type` must be `multipart/form-data` and the image file must be uploaded under the `image` key. You can also provide an optional name and description. Here is an example of the curl call:
  ```
  curl -F name="optionalFileName" -F description="An optional description" -F image=@./myFilePath.txt http://localhost:8080/api/rest/image/full
  # or just
  curl -F image=@./myFilePath.txt http://localhost:8080/api/rest/image/full
  ```

- `GET /api/rest/image` to list all images with their name, description, and downloadURL. Here is an example of the curl call:
  ```
  curl -X GET http://localhost:8080/api/rest/image
  ```

- `DELETE /api/rest/:id` to delete an uploaded image. `:id` should be an imageID. Here is an example of the curl call:
  ```
  curl -X DELETE http://localhost:8080/api/rest/imgae/92c77d46-0077-4d59-9d0c-a3c42ee785cd
  ```

- `GET /api/download/:id` to download an uploaded image. `id` should be an imageID. Here is an example of the curl call:
  ```
  curl http://localhost:8080/api/download/92c77d46-0077-4d59-9d0c-a3c42ee785cd
  ```

- `POST /api/rest/image` to create a new image without uploading it. Here is an example of the curl call:
  ```
  curl -X POST http://localhost:8080/api/rest/image -d '{"name": "curl", "description": "created with curl"}'
  ```

- `POST /api/upload/:id` to upload an already created image. The `Content-Type` must be `multipart/form-data`. Here is an example of the curl call:
  ```
  curl http://localhost:8080/api/upload/9e455c80-6660-43ad-85a1-4137f2f50ea5 -F image=@./myFilePath.jpg
  ```

## General

This golang project is based on Go Modules and mustn't be placed in GOPATH to avoid linter problems with imports.

This project is mainly a GraphQL server but everything can be used to build a REST API or a cronjob.

The `static/` folder is here to allow static web hosting.

All configurations are all in the `conf/` folder. All configurations can be put in different files. They will be watched for changes and the application will reload them. All services in the application can be notified of configuration change.

The `graphql/` folder contains all graphql files that contains the GraphQL Schema. These will be parsed and managed by [gqlgen](https://gqlgen.com/).

The project is linted by [GolangCI-Lint](https://golangci-lint.run/) and built by [Goreleaser](https://goreleaser.com/) or [Gox](https://github.com/mitchellh/gox).

The project have a convention in the coding strategy. All the business code related to your application specifically is located in a specific folder. They will be called business unit.

## Structure

The `cmd` folder contains all main packages.

The `pkg` folder contains all packages used by the application.

In this folder, there is:

- `pkg/../authx`: This folder contains packages related to authentication and authorization check.
- `pkg/../business`: This folder contains all business units of your application. These will contain services, models and data access object (dao) methods.
- `pkg/../common`: This folder contains common errors and utils used in all other packages.
- `pkg/../config`: This folder contains the package managing configuration. This provide a manager that give access to the last configuration loaded in the application. This allow to add hook for configuration reload.
- `pkg/../database`: This folder contains the package managing the SQL database connection and access.
- `pkg/../lockdistributor`: This contains a package that allow to acquire a distributed semaphore based on PostgreSQL.
- `pkg/../log`: This contains a package to have a logger.
- `pkg/../metrics`: This contains a package for metrics (Prometheus in this case).
- `pkg/../server`: This package contains servers code, GraphQL code and utils.
- `pkg/../tracing`: This package allow to have trace in the application using OpenTracing (implementation done with Jaeger).
- `pkg/../version`: This contains a package to have built version of the current application.

## Tools

Files for them are located in `tools/`.

### graphql-inspector

This tool is used in order to check is there is no breaking change in the GraphQL schema. This is done on pre-commit hooks via make receipts.

### voyager

This tool can be launched using NodeJS. This is a server launching [Voyager](https://github.com/APIs-guru/graphql-voyager).

To run this correctly, you must run the application server without any authentication system enabled and with some server CORS configurations:

```yaml
server:
  cors:
    useDefaultConfiguration: true
    allowWildcard: true
    allowCredentials: true
    allowOrigins:
      - http://*
```
